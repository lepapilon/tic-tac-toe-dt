package com.example.tictactoe;

import java.util.Arrays;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class GameActivity extends Activity {

	public static final int PLAYER_NONE = 0;
    public static final int PLAYER_1 = 1;
    public static final int PLAYER_2 = 2;
	
	public GameActivity() {
		gamePosition  = new int[3][3];
		
		for (int[] row: gamePosition)
			Arrays.fill(row, PLAYER_NONE);
		
		turn = PLAYER_1;
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.reset) {
        	gameReset();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void gameReset() {
    	System.out.println("gameReset");
    	
		for (int[] row: gamePosition)
			Arrays.fill(row, PLAYER_NONE);
		
		turn = PLAYER_1;
		gameEnded = false;
	    countMoves = 0;
		
		
		TextView spotReset = (TextView)findViewById(R.id.spot_1_1);
		spotReset.setBackgroundResource(R.drawable.blank);
		
		spotReset = (TextView)findViewById(R.id.spot_1_2);
		spotReset.setBackgroundResource(R.drawable.blank);

		spotReset = (TextView)findViewById(R.id.spot_1_3);
		spotReset.setBackgroundResource(R.drawable.blank);
		
		spotReset = (TextView)findViewById(R.id.spot_2_1);
		spotReset.setBackgroundResource(R.drawable.blank);
		
		spotReset = (TextView)findViewById(R.id.spot_2_2);
		spotReset.setBackgroundResource(R.drawable.blank);
		
		spotReset = (TextView)findViewById(R.id.spot_2_3);
		spotReset.setBackgroundResource(R.drawable.blank);
		
		spotReset = (TextView)findViewById(R.id.spot_3_1);
		spotReset.setBackgroundResource(R.drawable.blank);
		
		spotReset = (TextView)findViewById(R.id.spot_3_2);
		spotReset.setBackgroundResource(R.drawable.blank);
		
		spotReset = (TextView)findViewById(R.id.spot_3_3);
		spotReset.setBackgroundResource(R.drawable.blank);
	}
    
    public void gameMove(View view) 
    {
    	Context context = getApplicationContext();
    	int duration = Toast.LENGTH_LONG;
    	
    	// If the game has ended, do not let the user play anymore.
    	if(gameEnded)
    	{
    		Toast toast = Toast.makeText(context, "Your game has ended, please press reset to start a new one.", duration);
        	toast.show();
    		return;
    	}
    	
    	TextView spot = (TextView) view;
    	
    	int spotTag = Integer.parseInt(view.getTag().toString());
    	
    	int line = spotTag / 10;
    	int column = spotTag % 10;
    	
    	// Do not let the user click twice on the same spot.
    	if(gamePosition[line][column] == PLAYER_NONE)
    	{
    		if (turn == PLAYER_1) {
        		spot.setBackgroundResource(R.drawable.cross);
        	} else {
        		spot.setBackgroundResource(R.drawable.circle);
        	}
    		
    		gamePosition[line][column] = turn;
        	turn = (turn % 2) + 1;
        	countMoves++;
    	}

    	// If the game has less than 5 moves, it is not 
    	// possible to have a winner.
    	if(countMoves < 5)
    	{
    		return;
    	}
    	
    	//Check if the game has ended.
    	int winner = PLAYER_NONE;
    	
    	// Checking lines and columns
    	for(int i  = 0; i < 3; i++)
    	{
    		// Lines
    		if(gamePosition[i][0] != PLAYER_NONE)
    		{
    			if((gamePosition[i][0] == gamePosition[i][1]) && (gamePosition[i][0] == gamePosition[i][2]))
				{
    				winner = gamePosition[i][0];
	    			break;
				}

    		}
    		
    		// Columns
    		if(gamePosition[0][i] != PLAYER_NONE)
    		{
	    		if((gamePosition[0][i] == gamePosition[1][i]) && (gamePosition[0][i] == gamePosition[2][i]))
				{
	    			winner = gamePosition[0][i];
	    			break;
				}
    		}
    	}
    	
    	// Main diagonal
    	if(winner == PLAYER_NONE)
    	{
    		if(gamePosition[0][0] != PLAYER_NONE)
    		{
    			if((gamePosition[0][0] == gamePosition[1][1]) && (gamePosition[0][0] == gamePosition[2][2]))
				{
    				winner = gamePosition[0][0];
				}

    		}
    	}
    	
    	// Secondary diagonal
    	if(winner == PLAYER_NONE)
    	{
    		if(gamePosition[0][2] != PLAYER_NONE)
    		{
    			if((gamePosition[0][2] == gamePosition[1][1]) && (gamePosition[0][2] == gamePosition[2][0]))
				{
    				winner = gamePosition[0][2];
				}
    		}
    	}
    	
    	if(winner != PLAYER_NONE)
    	{
    		String result = String.format("Player %d has won!!\n", winner);
    		Toast toast = Toast.makeText(context, result, duration);
        	toast.show();
        	
    		gameEnded = true;
    		return;
    	}
    	
    	if(countMoves == 9)
    	{
    		Toast toast = Toast.makeText(context, "The game ended as a draw!", duration);
        	toast.show();
        	
    		gameEnded = true;
    	}
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_game, container, false);
            return rootView;
        }
    }
    
    private int turn;
    private int[][] gamePosition;
    private boolean gameEnded;
    private int countMoves;
}
